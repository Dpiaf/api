//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get("/", function(req, res){
  res.sendFile(path.join(__dirname, 'index.html')); //tomar en la raíz del proyecto
});
app.get("/clientes/:idcliente", function(req, res){
  res.send('Aquí tiene el cliente número :' + req.params.idcliente); //est se abre desde localhost:3000/clientes/<numero>
});
app.post("/", function(req, res){
  res.send('Hemos recibido su petición cambiada');
});
app.put("/", function(req, res){
  res.send('Hemos recibido su petición put');
});
app.delete("/", function(req, res){
  res.send('Hemos recibido su petición delete');
});
